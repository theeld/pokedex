import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComponent } from './list.component';
import { AppModule } from 'src/app/app.module';
import { APP_BASE_HREF } from '@angular/common';

describe('ListComponent', () => {
  let component: ListComponent;
	let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
			imports: [ AppModule ],
			declarations: [  ],
			providers: [{ provide: APP_BASE_HREF, useValue: '/' }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
		fixture = TestBed.createComponent(ListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

  it('should create', () => {
    expect(component).toBeTruthy();
	});

	it('should list results when enters component', (done) => {
		setTimeout(() => {
      expect(component.cards.length > 0).toBeTruthy()
      done();
    }, 2000);
	});

	it('should list results when enters component and search for a name', (done) => {
		component.applyFilter('mr. mime ')
		setTimeout(() => {
      expect(component.cards.length === 3).toBeTruthy()
      done();
    }, 3000);
	});
});
