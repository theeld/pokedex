import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { PokemonApiService } from 'src/app/core/services/pokemon-api.service';
import { Card } from 'pokemon-tcg-sdk-typescript/dist/sdk';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

	cards: Array<Card>
	cardsSubscription: Subscription

	@ViewChild(MatPaginator) paginator: MatPaginator
	page: number = 0
	pageSize: number = 10
	pageSizeOptions: number[] = [10, 25, 50]
	length: number = 1000
	name: string = ''


  constructor(private pokemonApi: PokemonApiService) { }

  ngOnInit() {
		this.fetchPokemonCards()
	}

	fetchPokemonCards(event?: any) {
		if (event) {
			this.page = event.pageIndex
			this.pageSize = event.pageSize
		}
		this.cardsSubscription = this.pokemonApi.getAllCards(String(this.page + 1), String(this.pageSize), this.name).subscribe(
			res => {
				this.length = Number(res['totalCount'])
				this.cards = res['cards'].sort((a, b) => (a.name > b.name) ? 1 : -1)
			}
		)
	}

	applyFilter(filterValue: string) {
		this.name = filterValue.toLowerCase()
		this.fetchPokemonCards()
	}

	eventTable(event) {
		this.fetchPokemonCards(event)
	}
	
	ngOnDestroy() {
		this.cardsSubscription.unsubscribe()
  }

}
