import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonApiService } from 'src/app/core/services/pokemon-api.service';
import { Card } from 'pokemon-tcg-sdk-typescript/dist/sdk';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

	id: string
	card: Card
	loading: boolean = true

  constructor(private activatedRoute: ActivatedRoute, private _pokemonApi: PokemonApiService) { }

  ngOnInit() {
		this.id = this.activatedRoute.snapshot.params['id']

		this._pokemonApi.getCardById(this.id).toPromise().then(
			res => {
				this.card = res['card']
				this.loading = false
			},
			error => {
				console.error(error)
				this.loading = false
			}
		)
  }

}
