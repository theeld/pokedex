import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { Card } from 'pokemon-tcg-sdk-typescript/dist/sdk';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


interface AllCards {
	totalCount: string,
	cards: Array<Card>
}

@Injectable({
	providedIn: 'root'
})
export class PokemonApiService {

	constructor(private _http: HttpClient) { }

	getAllCards(page: string, size: string, name: string = ''): Observable<AllCards> {

		let params = {
			'page': page,
			'pageSize': size,
			'name': name
		}

		const url = `${environment.pokemonApiURL}/${environment.pokemonApiVersion}/cards`
		return this._http.get(url, { observe: 'response', params: params }).pipe(
			map(res => {
				return { 'totalCount': res.headers.get('total-count'), 'cards': res.body['cards'] }
			},
				error => {
					return error
				})
		)

	}

	getCardById(id: string): Observable<any> {
		const url = `${environment.pokemonApiURL}/${environment.pokemonApiVersion}/cards/${id}`
		return this._http.get<any>(url)
	}
}
