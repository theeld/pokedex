import { TestBed, inject } from '@angular/core/testing';

import { PokemonApiService } from './pokemon-api.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppModule } from 'src/app/app.module';
import { APP_BASE_HREF } from '@angular/common';

describe('PokemonApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
			imports: [ AppModule ],
      providers: [PokemonApiService, HttpClient, { provide: APP_BASE_HREF, useValue: '/' }]
    });
  });

  it('should be created', inject([PokemonApiService], (service: PokemonApiService) => {
    expect(service).toBeTruthy();
  }));
});
