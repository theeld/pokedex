import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private _pkmdarkTheme = new Subject<boolean>();
  isDarkTheme = this._pkmdarkTheme.asObservable();

  setDarkTheme(isDarkTheme: boolean): void {
    this._pkmdarkTheme.next(isDarkTheme);
  }
}