import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ThemeService } from './services/theme.service';
import { PokemonApiService } from './services/pokemon-api.service';

import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  imports: [
		CommonModule,
		HttpClientModule
	],
	exports: [
		FlexLayoutModule,
		MatSlideToggleModule,
		MatToolbarModule,
		MatCardModule,
		MatChipsModule,
		MatPaginatorModule,
		MatInputModule
	],
	declarations: [],
	providers: [ ThemeService, PokemonApiService ]
})
export class CoreModule { }
