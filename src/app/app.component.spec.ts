import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CoreModule } from '@angular/flex-layout';
import { ThemeService } from './core/services/theme.service';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AppRoutingModule } from './app-routing.module';
import { ListComponent } from './pokemon/list/list.component';
import { DetailsComponent } from './pokemon/details/details.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatPaginatorModule } from '@angular/material/paginator';
import { APP_BASE_HREF } from '@angular/common';
describe('AppComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				AppComponent,
				ListComponent,
				DetailsComponent,
				PageNotFoundComponent
			],
			imports: [
				CoreModule,
				MatSlideToggleModule,
				MatToolbarModule,
				MatInputModule,
				MatCardModule,
				MatChipsModule,
				MatPaginatorModule,
				AppRoutingModule
			],
			providers: [ThemeService, { provide: APP_BASE_HREF, useValue: '/' }]
		}).compileComponents();
	}));
	it('should create the app', async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));
	it(`should have as title 'pokedex'`, async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app.title).toEqual('Pokedex');
	}));
	it('should render title in a h1 tag', async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('h1').textContent).toContain('Pokedex');
	}));
});
