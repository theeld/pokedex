import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ListComponent } from './pokemon/list/list.component';
import { DetailsComponent } from './pokemon/details/details.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { environment } from 'src/environments/environment';

const appRoutes: Routes = [
	{ path: 'list', component: ListComponent },
	{ path: 'details/:id', component: DetailsComponent },
	{ path: '', redirectTo: '/list', pathMatch: 'full' },
	{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
	imports: [
		RouterModule.forRoot(
			appRoutes,
			{ enableTracing: !environment.production } // <-- debugging purposes only
		)
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule { }
