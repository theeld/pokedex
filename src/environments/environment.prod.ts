export const environment = {
	production: true,
	pokemonApiURL: 'https://api.pokemontcg.io',
	pokemonApiVersion: 'v1'
};
